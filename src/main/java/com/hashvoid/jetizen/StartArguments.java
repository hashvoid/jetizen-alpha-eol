/*
 * Copyright (c) The original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package com.hashvoid.jetizen;

import java.util.HashMap;
import java.util.Map;

/**
 * @author randondiesel
 *
 */

class StartArguments {

	private Map<String, String[]> arguments;

	public StartArguments(String[] args) {
		arguments = new HashMap<>();

		if(args == null) {
			return;
		}

		String lastKey = null;
		String[] lastValues = new String[0];
		for(int i=0; i<args.length; i++) {
			if(args[i].length() > 1 && args[i].charAt(0) == '-') {
				if(lastKey != null) {
					arguments.put(lastKey, lastValues);
				}
				lastKey = args[i].substring(1);
				lastValues = new String[0];
			}
			else {
				if(lastKey != null) {
					lastValues = appendToArray(lastValues, args[i]);
				}
			}
		}

		if(lastKey != null) {
			arguments.put(lastKey, lastValues);
		}
	}

	public boolean contains(String key) {
		return arguments.containsKey(key);
	}

	public int countValues(String key) {
		if(!arguments.containsKey(key)) {
			return -1;
		}
		return arguments.get(key).length;
	}

	public String[] getValues(String key) {
		if(arguments.containsKey(key)) {
			return arguments.get(key);
		}
		return new String[0];
	}

	////////////////////////////////////////////////////////////////////////////////////////////////
	// Helper methods

	private String[] appendToArray(String[] original, String item) {
		String[] result = null;
		if(original == null) {
			result = new String[1];
			result[0] = item;
		}
		else {
			result = new String[original.length + 1];
			System.arraycopy(original, 0, result, 0, original.length);
			result[original.length] = item;
		}
		return result;
	}
}
