/*
 * Copyright (c) The original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package com.hashvoid.jetizen;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.hashvoid.amberjack.servlet.AmberjackServlet;
import com.hashvoid.amberjack.servlet.ServletAmberRequest;
import com.hashvoid.amberjack.servlet.ServletAmberResponse;

/**
 * @author randondiesel
 *
 */

class JetizenServlet extends AmberjackServlet {

	private static final long serialVersionUID = -7920323727970453255L;

	private List<ContextEntityProvider> cepList;

	public void addContextEntityProvider(ContextEntityProvider cep) {
		cepList.add(cep);
	}

	////////////////////////////////////////////////////////////////////////////////////////////////
	// Methods of base class AmberjackServlet

	@Override
	public void init() throws ServletException {
		super.init();
		cepList = new ArrayList<>();
	}

	@Override
	protected void beforeExecute(HttpServletRequest req, HttpServletResponse resp,
			ServletAmberRequest areq, ServletAmberResponse aresp)
			throws ServletException, IOException {
		for(ContextEntityProvider cep : cepList) {
			Object entity = cep.getEntity(req, resp);
			areq.addContext(entity);
		}
	}
}
