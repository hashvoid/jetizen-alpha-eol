/*
 * Copyright (c) The original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package com.hashvoid.jetizen;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.Context;

/**
 * This interface is used by Jetizen to obtain, on a per HTTP request basis, objects that are made
 * available to Web resources and methods as entities that are annotated with {@link Context}. Refer
 * to the JSR 339 specification on the exact details of context entities.
 * <p>
 *
 * @author randondiesel
 *
 */

public interface ContextEntityProvider {

	Object getEntity(HttpServletRequest req, HttpServletResponse resp);
}
