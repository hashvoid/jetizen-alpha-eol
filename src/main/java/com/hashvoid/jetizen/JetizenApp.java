/*
 * Copyright (c) The original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package com.hashvoid.jetizen;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.net.InetSocketAddress;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.ServletContext;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.ContextHandlerCollection;
import org.eclipse.jetty.servlet.DefaultServlet;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;

import com.hashvoid.amberjack.ext.JsonMessageBodyReader;
import com.hashvoid.amberjack.ext.JsonMessageBodyWriter;
import com.hashvoid.amberjack.servlet.AmberScanPath;
import com.hashvoid.conn.support.ConnContextKeyNames;
import com.hashvoid.conn.support.ConnServlet;
import com.hashvoid.crossbinder.hod.Crossbinder;
import com.hashvoid.jetizen.config.AdminConfig;
import com.hashvoid.jetizen.config.AmberjackConfig;
import com.hashvoid.jetizen.config.ServerConfig;
import com.hashvoid.jetizen.config.StaticSiteConfig;
import com.hashvoid.jsonmapper.decode.Json2Object;

/**
 * @author randondiesel
 *
 */

public abstract class JetizenApp<T extends JetizenConfig> {

	private static final Logger LOGGER = Logger.getLogger(JetizenApp.class.getName());

	private StartArguments           startArgs;
	private T                        config;
	private Crossbinder              xbind;
	private Server                   jettyServer;
	private ContextHandlerCollection contexts;
	private Server                   adminServer;
	private ServletContext           amberServletCtxt;
	private JetizenServlet           servlet;

	public final void run(String... args) {
		LOGGER.info("Jetizen: Fast bootstrapping of Java REST services.");

		//parse the arguments (expected to be command line) into a map structure.
		startArgs = new StartArguments(args);
		//load the config file.
		loadConfig();

		//Prepare the crossbinder
		xbind = Crossbinder.create();
		if(config.getClasspathScanConfig() == null) {
			throw new RuntimeException("classpath scan configuration is missing");
		}
		config.getClasspathScanConfig().prepare();
		config.getClasspathScanConfig().configureCrossbinder(xbind);

		initServer();
		initAdminServer();

		initialize();

		//Create a crossbinder configuration provider using the loaded JetizenConfig
		CrossbinderConfigAdapter cca = new CrossbinderConfigAdapter(config);
		//Register the configuration provider with crossbinder
		xbind.configure(cca);
		//Start crossbinder
		xbind.start();

		try {
			if(adminServer != null) {
				adminServer.start();
				LOGGER.info("admin server is up.");
			}
			jettyServer.start();
			LOGGER.info("web server is up.");
		}
		catch (Exception exep) {
			throw new RuntimeException("unable to start jetty server", exep);
		}
	}

	public final void stop() {
		try {
			jettyServer.stop();
		}
		catch (Exception exep) {
			throw new RuntimeException("unable to stop jetty server", exep);
		}

		try {
			if(adminServer != null) {
				adminServer.stop();
			}
		}
		catch (Exception exep) {
			throw new RuntimeException("unable to stop jetty server", exep);
		}

		if(xbind != null) {
			xbind.stop();
		}

		dispose();
	}

	protected final StartArguments getStartArguments() {
		return startArgs;
	}

	public final T getConfiguration() {
		/*
		 * Note: this method must be left public. Else, we will not be able to determine the correct
		 * configuration sub-class through introspection.
		 */
		return config;
	}

	protected final Crossbinder getCrossbinder() {
		return xbind;
	}

/**
 * Retrieves the servlet context instance that is associated with the Amberjack container.
 * <p>
 *
 * @return the amberjack servlet context.
 */

	protected final ServletContext getServletContext() {
		return amberServletCtxt;
	}

	protected final Server getJettyServer() {
		return jettyServer;
	}

	protected final void addContextEntityProvider(ContextEntityProvider provider) {
		servlet.addContextEntityProvider(provider);
	}

	protected abstract void initialize();

	protected abstract void dispose();

	////////////////////////////////////////////////////////////////////////////////////////////////
	// Helper methods

	@SuppressWarnings("unchecked")
	private void loadConfig() {
		if(!startArgs.contains("config")) {
			throw new RuntimeException("missing startup argument -config");
		}
		String[] values = startArgs.getValues("config");
		if(values.length != 1) {
			throw new RuntimeException("missing or incorrect number of arguments to -config");
		}
		String configFileName = values[0];

		byte[] jsonData = null;
		try {
			FileInputStream fis = new FileInputStream(configFileName);
			ByteArrayOutputStream buffer = new ByteArrayOutputStream();
			byte[] block = new byte[1024];
			int amount = -1;
			while((amount = fis.read(block)) >= 0) {
				buffer.write(block, 0, amount);
			}
			buffer.flush();
			fis.close();
			jsonData = buffer.toByteArray();
		}
		catch(IOException exep) {
			throw new RuntimeException("unable to load json config from " + configFileName, exep);
		}

		Class<?> configType = getConfigType();
		if(configType == null) {
			throw new RuntimeException("unable to populate config. Cannot determine config type");
		}
		LOGGER.fine(String.format("config type is: %s", configType.getName()));
		config = (T) new Json2Object().convert(jsonData, configType);
	}

	private void initServer() {
		ServerConfig scon = config.getServerConfig();
		if(scon == null) {
			LOGGER.info("server configuration is missing. Going with defaults.");
			scon = new ServerConfig();
		}
		LOGGER.info(String.format("web server will be started on %s:%d", scon.getHost(), scon.getPort()));
		jettyServer = new Server(new InetSocketAddress(scon.getHost(), scon.getPort()));
		contexts = new ContextHandlerCollection();
		jettyServer.setHandler(contexts);

		initSites(scon);
		initAmberjack(scon);
	}

	private void initSites(ServerConfig scon) {
		LOGGER.info("initializing static sites");
		List<StaticSiteConfig> ssconList = scon.getStaticSites();
		if(ssconList == null || ssconList.isEmpty()) {
			LOGGER.info("no static sites have been configured.");
			return;
		}
		for(StaticSiteConfig sscon : ssconList) {
			initSite0(sscon);
		}
	}

	private void initSite0(StaticSiteConfig sscon) {
		ServletContextHandler sch = new ServletContextHandler();
		sch.setContextPath(sscon.getPath());

		ServletHolder holder = new ServletHolder("ds", DefaultServlet.class);
		holder.setInitOrder(0);
		holder.setInitParameter("resourceBase", sscon.getRealPath());
		holder.setInitParameter("dirListing", Boolean.toString(sscon.isDirListing()));
		holder.setInitParameter("acceptRanges", Boolean.toString(sscon.isAcceptRanges()));
		sch.addServlet(holder, "/*");

		contexts.addHandler(sch);
		LOGGER.info(String.format("static_site %s --> %s ; dir_listing_allowed = %s",
				sscon.getPath(), sscon.getRealPath(), sscon.isDirListing()));
	}

	@SuppressWarnings("unchecked")
	private void initAmberjack(ServerConfig scon) {
		AmberjackConfig ajcon = scon.getAmberjackConfig();
		if(ajcon == null) {
			LOGGER.info("amberjack configuration is missing. Going with defaults.");
			ajcon = new AmberjackConfig();
		}

		AmberScanPath asp = config.getClasspathScanConfig().getAmberScanPath();
		if(ajcon.isJsonMessageBodyConvertersEnabled()) {
			asp.includedClasses().add(JsonMessageBodyReader.class.getName());
			asp.includedClasses().add(JsonMessageBodyWriter.class.getName());
		}

		String ctxtPath = ajcon.getPath();
		ServletContextHandler handler = new ServletContextHandler();

		Class<Application> appCls = null;
		if(ajcon.getApplicationClass() != null) {
			try {
				appCls = (Class<Application>) Class.forName(ajcon.getApplicationClass());
			}
			catch (ClassNotFoundException | ClassCastException exep) {
				throw new RuntimeException("JAXRS application class verification failed", exep);
			}
		}

		servlet = new JetizenServlet();
		servlet.setDependencyInjector(new CrossbinderInjector(xbind));

		if(appCls != null) {
			ApplicationPath appPath = appCls.getAnnotation(ApplicationPath.class);
			if(appPath != null) {
				ctxtPath = appPath.value();
			}

			ServletHolder holder = new ServletHolder(appCls.getName(), servlet);
			holder.setInitParameter("javax.ws.rs.Application", appCls.getName());
			holder.setInitOrder(0);
			handler.addServlet(holder, "/*");
		}
		else {
			ServletHolder holder = new ServletHolder("javax.ws.rs.core.Application", servlet);
			servlet.setAmberScanPath(config.getClasspathScanConfig().getAmberScanPath());

			holder.setInitOrder(0);
			handler.addServlet(holder, "/*");
		}

		//set the handler context path.
		if(!ctxtPath.startsWith("/")) {
			ctxtPath = "/" + ctxtPath;
		}
		if(ctxtPath.endsWith("/")) {
			ctxtPath = ctxtPath.substring(0, ctxtPath.length() - 1);
		}
		handler.setContextPath(ctxtPath);
		//save the associated servlet context for access from within a sub-class
		amberServletCtxt = handler.getServletContext();

		contexts.addHandler(handler);
	}

	private void initAdminServer() {
		AdminConfig adcon = config.getAdminConfig();
		if(adcon == null) {
			LOGGER.info("admin configuration not found. Server will not be started");
			return;
		}
		LOGGER.info(String.format("admin server will be started on %s:%d", adcon.getHost(), adcon.getPort()));
		adminServer = new Server(new InetSocketAddress(adcon.getHost(), adcon.getPort()));
		ServletContextHandler handler = new ServletContextHandler();
		handler.setContextPath(adcon.getPath());
		adminServer.setHandler(handler);

		ServletHolder holder = new ServletHolder("connServlet", ConnServlet.class);
		holder.setInitOrder(0);
		handler.addServlet(holder, "/*");
		handler.getServletContext().setAttribute(ConnContextKeyNames.CONN_SCANPATH,
				config.getClasspathScanConfig().getConnScanPath());
		handler.getServletContext().setAttribute(ConnContextKeyNames.CONN_DEPENDENCY_INJECTOR,
				new CrossbinderInjector(xbind));
	}

	////////////////////////////////////////////////////////////////////////////////////////////////
	// Helper utility method

	private Class<?> getConfigType() {
		ParameterizedType ptype = null;
		Class<?> cls = getClass();
		while(cls != null) {
			Type type = cls.getGenericSuperclass();
			if(type == null) {
				break;
			}
			if(type instanceof ParameterizedType) {
				ptype = (ParameterizedType) type;
				break;
			}
			cls = cls.getSuperclass();
		}
		if(ptype == null) {
			return null;
		}

		Type[] typeArgs = ptype.getActualTypeArguments();
		if(typeArgs == null || typeArgs.length != 1) {
			return null;
		}
		if(!(typeArgs[0] instanceof Class)) {
			return null;
		}
		return (Class<?>) typeArgs[0];
	}
}
