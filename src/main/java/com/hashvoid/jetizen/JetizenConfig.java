/*
 * Copyright (c) The original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package com.hashvoid.jetizen;

import com.hashvoid.jetizen.config.AdminConfig;
import com.hashvoid.jetizen.config.ClasspathScanConfig;
import com.hashvoid.jetizen.config.ServerConfig;
import com.hashvoid.jsonmapper.JSON;

/**
 * @author randondiesel
 *
 */

public class JetizenConfig {

	@JSON("server")
	private ServerConfig serverConfig;

	@JSON("admin-server")
	private AdminConfig adminConfig;

	@JSON("classpath-scan")
	private ClasspathScanConfig scanConfig;

	public final ServerConfig getServerConfig() {
		return serverConfig;
	}

	public final AdminConfig getAdminConfig() {
		return adminConfig;
	}

	public final ClasspathScanConfig getClasspathScanConfig() {
		return scanConfig;
	}
}
