/*
 * Copyright (c) The original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package com.hashvoid.jetizen;

import java.lang.reflect.Field;
import java.util.logging.Logger;

import com.hashvoid.crossbinder.hod.ConfigurationProvider;
import com.hashvoid.jsonmapper.JSON;

/**
 *
 * @author randondiesel
 *
 */

class CrossbinderConfigAdapter implements ConfigurationProvider {

	private static final Logger LOGGER = Logger.getLogger(CrossbinderConfigAdapter.class.getName());

	private JetizenConfig dwConfig;

	public CrossbinderConfigAdapter(JetizenConfig config) {
		dwConfig = config;
	}

	////////////////////////////////////////////////////////////////////////////
	// Methods of interface ConfigurationProvider

	@Override
	public boolean contains(String path) {
		boolean result = false;
		try {
			result = (getValueRecursive(path, dwConfig) != null);
		}
		catch (Exception exep) {
			//NOOP
		}
		LOGGER.fine(String.format("checking configuration [%s], found = %b", path, result));
		return result;
	}

	@Override
	public Object getValue(String path, Class<?> type) {
		Object value = null;
		try {
			value = getValueRecursive(path, dwConfig);
		}
		catch (Exception exep) {
			return null;
		}
		if(value == null) {
			return null;
		}

		try {
			if(type.equals(Integer.TYPE)) {
				return Integer.parseInt(value.toString());
			}
			if(type.equals(Float.TYPE)) {
				return Float.parseFloat(value.toString());
			}
			if(type.equals(Long.TYPE)) {
				return Long.parseLong(value.toString());
			}
			if(type.equals(Double.TYPE)) {
				return Double.parseDouble(value.toString());
			}
			if(type.equals(Boolean.TYPE)) {
				return new Boolean(value.toString());
			}
			if(type.equals(Byte.TYPE)) {
				return Byte.parseByte(value.toString());
			}
		}
		catch(Exception exep) {
			return null;
		}

		if(type.isAssignableFrom(value.getClass())) {
			return value;
		}
		return null;
	}

	////////////////////////////////////////////////////////////////////////////
	// Helper methods

	private Object getValueRecursive(String path, Object inst) throws Exception {
		String[] parts = path.split("\\.", 2);
		Object value = getValueFromFields(parts[0], inst.getClass(), inst);
		if(value != null) {
			if(parts.length > 1) {
				return getValueRecursive(parts[1], value);
			}
			else {
				return value;
			}
		}
		return null;
	}

	private Object getValueFromFields(String path, Class<?> type, Object inst) throws Exception {
		if(path == null || path.trim().isEmpty()) {
			return null;
		}
		Field[] fields = type.getDeclaredFields();
		for(Field field : fields) {
			JSON ann = field.getAnnotation(JSON.class);
			if(ann != null) {
				String annName = ann.value();
				if(annName == null || annName.trim().isEmpty()) {
					annName = field.getName();
				}
				else {
					annName = annName.trim();
				}
				if(path.equals(annName)) {
					boolean accessible = field.isAccessible();
					if(!accessible) {
						field.setAccessible(true);
					}
					Object value = field.get(inst);
					if(!accessible) {
						field.setAccessible(false);
					}
					return value;
				}
			}
		}
		return null;
	}
}
