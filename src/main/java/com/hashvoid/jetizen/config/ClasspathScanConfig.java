/*
 * Copyright (c) The original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package com.hashvoid.jetizen.config;

import java.util.List;

import com.hashvoid.amberjack.servlet.AmberScanPath;
import com.hashvoid.conn.support.ConnScanPath;
import com.hashvoid.crossbinder.hod.Crossbinder;
import com.hashvoid.jsonmapper.JSON;

/**
 * @author randondiesel
 *
 */

public class ClasspathScanConfig {

	@JSON("classpaths")
	private List<String> classPaths;

	@JSON("libpaths")
	private List<String> libPaths;

	@JSON("include-packages")
	private List<String> includePkgs;

	@JSON("exclude-packages")
	private List<String> excludePkgs;

	@JSON("include-classes")
	private List<String> includeClasses;

	@JSON("exclude-classes")
	private List<String> excludeClasses;

	@JSON("include-patterns")
	private List<String> includePats;

	@JSON("exclude-patterns")
	private List<String> excludePats;

	private AmberScanPath amberScanPath;
	private ConnScanPath  connScanPath;

	public void prepare() {
		amberScanPath = new AmberScanPath();
		connScanPath = new ConnScanPath();
		if(classPaths != null) {
			amberScanPath.classPaths().addAll(classPaths);
			connScanPath.classPaths().addAll(classPaths);
		}

		if(libPaths != null) {
			amberScanPath.libPaths().addAll(libPaths);
			connScanPath.libPaths().addAll(libPaths);
		}

		if(includePkgs != null) {
			amberScanPath.includedPackages().addAll(includePkgs);
			connScanPath.includedPackages().addAll(includePkgs);
		}

		if(excludePkgs != null) {
			amberScanPath.excludedPackages().addAll(excludePkgs);
			connScanPath.excludedPackages().addAll(excludePkgs);
		}

		if(includeClasses != null) {
			amberScanPath.includedClasses().addAll(includeClasses);
			connScanPath.includedClasses().addAll(includeClasses);
		}

		if(excludeClasses != null) {
			amberScanPath.excludedClasses().addAll(excludeClasses);
			connScanPath.excludedClasses().addAll(excludeClasses);
		}

		if(includePats != null) {
			amberScanPath.includedClassesByPattern().addAll(includePats);
			connScanPath.includedClassesByPattern().addAll(includePats);
		}

		if(excludePats != null) {
			amberScanPath.excludedClassesByPattern().addAll(excludePats);
			connScanPath.excludedClassesByPattern().addAll(excludePats);
		}
	}

	public void configureCrossbinder(Crossbinder cb) {
		if(classPaths != null) {
			for(String path : classPaths) {
				cb.scanner().addClassPath(path);
			}
		}

		if(libPaths != null) {
			for(String path : libPaths) {
				cb.scanner().addLibPath(path);
			}
		}

		if(includePkgs != null) {
			for(String pkg : includePkgs) {
				cb.scanner().includePackage(pkg);
			}
		}

		if(excludePkgs != null) {
			for(String pkg : excludePkgs) {
				cb.scanner().excludePackage(pkg);
			}
		}

		if(includeClasses != null) {
			cb.scanner().includeClasses(includeClasses.toArray(new String[0]));
		}

		if(excludeClasses != null) {
			cb.scanner().excludeClasses(excludeClasses.toArray(new String[0]));
		}

		if(includePats != null) {
			for(String pat : includePats) {
				cb.scanner().includeClassesByPattern(pat);
			}
		}

		if(excludePats != null) {
			for(String pat : excludePats) {
				cb.scanner().excludeClassesByPattern(pat);
			}
		}
	}

	public AmberScanPath getAmberScanPath() {
		return amberScanPath;
	}

	public ConnScanPath getConnScanPath() {
		return connScanPath;
	}
}
