/*
 * Copyright (c) The original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package com.hashvoid.jetizen.config;

import java.util.List;

import com.hashvoid.jsonmapper.JSON;

/**
 * @author randondiesel
 *
 */

public class ServerConfig {

	@JSON("host")
	private String host;

	@JSON("port")
	private int port;

	@JSON("static-sites")
	private List<StaticSiteConfig> staticSites;

	@JSON("amberjack")
	private AmberjackConfig amberConfig;

	public String getHost() {
		if(host == null || host.trim().length() == 0) {
			return "0.0.0.0";
		}
		return host.trim();
	}

	public int getPort() {
		if(port <= 0) {
			return 8080;
		}
		return port;
	}

	public List<StaticSiteConfig> getStaticSites() {
		return staticSites;
	}

	public AmberjackConfig getAmberjackConfig() {
		return amberConfig;
	}
}
