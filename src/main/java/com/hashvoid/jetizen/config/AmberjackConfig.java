/*
 * Copyright (c) The original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package com.hashvoid.jetizen.config;

import com.hashvoid.jsonmapper.JSON;

/**
 * @author randondiesel
 *
 */

public class AmberjackConfig {

	@JSON("path")
	private String path;

	@JSON("application-class")
	private String appCls;

	@JSON("disable-json-messagebody-converters")
	private boolean disableJsonConv;

	public String getPath() {
		if(path == null || path.trim().length() == 0) {
			return "/";
		}
		return path;
	}

	public String getApplicationClass() {
		return appCls;
	}

	public boolean isJsonMessageBodyConvertersEnabled() {
		return !disableJsonConv;
	}
}
